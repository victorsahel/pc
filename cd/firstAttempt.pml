byte turn = 1

active proctype P() {
    byte i = 0
    do
    :: i < 25 ->
        printf("Proceso%d non critical section 1\n", _pid)
        printf("Proceso%d non critical section 2\n", _pid)
        printf("Proceso%d non critical section 3\n", _pid)
        (turn == 1) ->
        printf("Proceso%d CRITICAL SECTION 1\n", _pid)
        printf("Proceso%d CRITICAL SECTION 2\n", _pid)
        printf("Proceso%d CRITICAL SECTION 3\n", _pid)
        turn = 2
        i++
    :: else -> break
    od
}

active proctype Q() {
    byte i = 0
    do
    :: i < 25 ->
        printf("Proceso%d non critical section 1\n", _pid)
        printf("Proceso%d non critical section 2\n", _pid)
        printf("Proceso%d non critical section 3\n", _pid)
        (turn == 2) ->
        printf("Proceso%d CRITICAL SECTION 1\n", _pid)
        printf("Proceso%d CRITICAL SECTION 2\n", _pid)
        printf("Proceso%d CRITICAL SECTION 3\n", _pid)
        turn = 1
        i++
    :: else -> break
    od
}
