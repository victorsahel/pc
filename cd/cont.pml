byte n = 0

proctype P() {
    byte t
    byte i = 0
    do
    :: i < 10 ->
        t = n
        n = t + 1
        i++
    :: else -> break
    od
}

init {
    atomic {
        run P();
        run P()
    }
    (_nr_pr == 1) -> printf("N = %d\n", n)
}
