bool wantp = false
bool wantq = false

active proctype P() {
    do
    :: true ->
        printf("Proceso%d non critical section 1\n", _pid)
        printf("Proceso%d non critical section 2\n", _pid)
        printf("Proceso%d non critical section 3\n", _pid)
        wantp = true
        (!wantq) ->
        printf("Proceso%d CRITICAL SECTION 1\n", _pid)
        printf("Proceso%d CRITICAL SECTION 2\n", _pid)
        printf("Proceso%d CRITICAL SECTION 3\n", _pid)
        wantp = false
    od
}

active proctype Q() {
    do
    :: true ->
        printf("Proceso%d non critical section 1\n", _pid)
        printf("Proceso%d non critical section 2\n", _pid)
        printf("Proceso%d non critical section 3\n", _pid)
        wantq = true
        (!wantp) ->
        printf("Proceso%d CRITICAL SECTION 1\n", _pid)
        printf("Proceso%d CRITICAL SECTION 2\n", _pid)
        printf("Proceso%d CRITICAL SECTION 3\n", _pid)
        wantq = false
    od
}
